<?php

// Exibir erros do PHP
ini_set('display_errors', 1);
error_reporting(E_ALL);

require_once 'core/Application.php';

// Inicia a aplicação
$objApp = new Application();
$objApp->dispatch();