<?php

function autoload($class){
  if (file_exists('core/'.$class.'.php')) {
    require_once 'core/'.$class.'.php';
  }
}

spl_autoload_register("autoload");

class Application {

  protected $controller;
  protected $action;

  private function loadRoute() {
    $this->controller = $_REQUEST['controller'] ?? 'Tarefa';
    $this->action     = $_REQUEST['action'] ?? 'index';
  }

  public function dispatch() {
    $this->loadRoute();

    $controller = 'controllers/' . $this->controller . 'Controller.php';

    if (file_exists($controller)) {
      require_once $controller;
    } else {
      throw new Exception('Controller "' . $this->controller . '" não existe!');
    }

    $class = $this->controller.'Controller';

    if (class_exists($class)) {
      $objClass = new $class;
    } else {
      throw new Exception('Classe "' . $class . '" não existe no controller "' . $this->controller . '"');
    }

    $method = $this->action;

    if (method_exists($objClass, $method)) {
      $objClass->$method();
    } else {
      throw new Exception('Método "' . $method . '" nao existe na classe "' . $class . '"');
    }
  }

}