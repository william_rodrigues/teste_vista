<?php

class Helper {

  static function isNumeric($value){

    $value = str_replace(',', '.', $value);

    if(!is_numeric($value)){
      return false;
    }

    return true;
  }

  static function isInteger($value){

    if(!Helper::isNumeric($value)){
      return false;
    }

    if(preg_match('/[[:punct:]&^-]/', $value) > 0){
      return false;
    }

    return true;
  }

  static function cleanString($string){
    return addslashes(strip_tags($string));
  }
}