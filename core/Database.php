<?php

class Database {

  protected $mysqli;

  function __construct() {
    $hostname = 'localhost';
    $username = 'root';
    $password = '';
    $database = 'teste_vista';

    $this->mysqli = new mysqli($hostname, $username, $password, $database);
    $this->mysqli->set_charset("utf8");
  }

}