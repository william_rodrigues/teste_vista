<?php

class View {

  private $file;
  private $params;

  function __construct($view_name = null, $view_params = null) {
    $this->file   = 'views/'.$view_name.'.php';
    $this->params = $view_params;
  }

  public function showView() {

    ob_start();

    if(file_exists($this->file)){
      require_once $this->file;
    } else {
      throw new Exception("View File don't exists");
    }

    $view_content = ob_get_contents();

    ob_end_clean();

    echo $view_content;
    exit;
  }

}