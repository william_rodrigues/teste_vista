<!DOCTYPE html>
<html lang="pt-BR">

<head>

  <title>Teste - Vista Soft</title>

  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" type="text/css" rel="stylesheet"  media="screen,projection,print"/>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">

</head>
<body>
  <div class="container">

    <div class="row">
      <div class="col-md-12">

        <h1 class="text-center my-4 py-3 alert-secondary">Lista de Tarefas</h1>

        <table id="tarefas" class="table table-striped table-bordered table-hover text-center">
          <thead class="thead-dark">
            <th>#</th>
            <th>Prioridade</th>
            <th>Prazo</th>
            <th>Título</th>
            <th>Responsável</th>
            <th>Status</th>
            <th>Ações</th>
          </thead>

          <?php $cor_prioridade = array('ALTA' => 'danger', 'MEDIA' => 'warning', 'BAIXA' => 'info'); ?>

          <tbody>
            <?php foreach ($this->params['tarefas'] as $tarefa) { ?>
              <tr>
                <td class="align-middle"><?php echo $tarefa->getId() ?></td>
                <td class="align-middle"><div class="m-0 alert btn-<?php echo $cor_prioridade[$tarefa->getPrioridade()] ?>"><?php echo $tarefa->getPrioridade() ?></div></td>
                <td class="align-middle"><?php echo date('d/m/Y', strtotime($tarefa->getPrazo())) ?></td>
                <td class="align-middle"><?php echo $tarefa->getTitulo() ?></td>
                <td class="align-middle"><?php echo $tarefa->getNomeResponsavel() ?></td>
                <td class="align-middle"><?php echo $tarefa->getStatus() ?></td>
                <td class="align-middle" align="center">
                  <a href='?controller=Tarefa&action=form&id=<?php echo $tarefa->getId() ?>'><i class="fas fa-edit" title="Editar"></i></a>
                  <a href='?controller=Tarefa&action=deletar&id=<?php echo $tarefa->getId() ?>'><i class="fas fa-trash-alt" title="Excluir"></i></a>
                </td>
              </tr>
            <?php } ?>
          </tbody>

        </table>

        <div class="float-right m-2"><a href='?controller=Tarefa&action=form'><button type="button" class="btn btn-success">Nova Tarefa</button></a></div>

      </div>
    </div>
  </div>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  <script src="http://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

  <script type="text/javascript">
    $(document).ready(function(){
      $('#tarefas').DataTable({
        searching: false,
        paging: false,
        orderable: [],
        "columns": [
          { "width": "5%" },
          { "orderable": false, "width": "5%" },
          { "orderable": false, "width": "10%" },
          { "orderable": false, "width": "40%" },
          { "orderable": false, "width": "20%" },
          { "orderable": false, "width": "15%" },
          { "orderable": false, "width": "5%" },
        ],
        "language": {
          "sEmptyTable": "Nenhum registro encontrado",
          "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
          "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
          "sInfoFiltered": "(Filtrados de _MAX_ registros)",
          "sInfoPostFix": "",
          "sInfoThousands": ".",
          "sLengthMenu": "_MENU_ resultados por página",
          "sLoadingRecords": "Carregando...",
          "sProcessing": "Processando...",
          "sZeroRecords": "Nenhum registro encontrado",
          "sSearch": "Pesquisar",
          "oPaginate": {
              "sNext": "Próximo",
              "sPrevious": "Anterior",
              "sFirst": "Primeiro",
              "sLast": "Último"
          },
          "oAria": {
              "sSortAscending": ": Ordenar colunas de forma ascendente",
              "sSortDescending": ": Ordenar colunas de forma descendente"
          }
        }
      });
    });
  </script>

</body>
</html>