<!DOCTYPE html>
<html lang="pt-BR">

<head>

  <title>Teste - Vista Soft</title>

  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

</head>
<body>

    <div class="container">

    <div class="row">
      <div class="col-md-12">
        <h1 class="text-center my-4 py-3 alert-secondary">Cadastro de Tarefas</h1>
      </div>
    </div>

    <div class="row">
      <form method="post" class="col-md-12">

        <div class="row">
          <div class="col-md-12 form-group">

            <div class="col-md-6 float-left">
              <label for="titulo">Título</label>
              <input id="titulo" type="text" class="form-control" name="titulo" value="<?php echo $this->params['tarefa']->getTitulo() ?>">
            </div>

            <div class="col-md-6 float-left">
              <label for="nome_responsavel">Nome do Responsável</label>
              <input id="nome_responsavel" type="text" class="form-control" name="nome_responsavel" value="<?php echo $this->params['tarefa']->getNomeResponsavel() ?>">
            </div>

          </div>
        </div>

        <div class="row">
          <div class="col-md-12 form-group">
            <div class="col-md-4 float-left">
              <label for="prazo">Prazo</label>
              <input id="prazo" type="date" class="form-control" name="prazo" value="<?php echo $this->params['tarefa']->getPrazo() ?>">
            </div>

            <div class="col-md-4 float-left">
              <label>Status:</label>
              <select class="form-control" name="status">
                <option value="PENDENTE" <?php echo ($this->params['tarefa']->getStatus() == "PENDENTE") ? 'selected' : '' ?>>PENDENTE</option>
                <option value="EM ANDAMENTO" <?php echo ($this->params['tarefa']->getStatus() == "EM ANDAMENTO") ? 'selected' : '' ?>>EM ANDAMENTO</option>
                <option value="FINALIZADO" <?php echo ($this->params['tarefa']->getStatus() == "FINALIZADO") ? 'selected' : '' ?>>FINALIZADO</option>
                <option value="CANCELADO" <?php echo ($this->params['tarefa']->getStatus() == "CANCELADO") ? 'selected' : '' ?>>CANCELADO</option>
              </select>
            </div>

            <div class="col-md-4 float-left">
              <label>Prioridade:</label>
              <select class="form-control" name="prioridade">
                <option value="BAIXA" <?php echo ($this->params['tarefa']->getPrioridade() == "BAIXA") ? 'selected' : '' ?>>BAIXA</option>
                <option value="MEDIA" <?php echo ($this->params['tarefa']->getPrioridade() == "MEDIA") ? 'selected' : '' ?>>MEDIA</option>
                <option value="ALTA" <?php echo ($this->params['tarefa']->getPrioridade() == "ALTA") ? 'selected' : '' ?>>ALTA</option>
              </select>
            </div>
          </div>
        </div>

        <input type="hidden" name="controller" value="Tarefa">
        <input type="hidden" name="action" value="form">
        <input type="hidden" name="id" value="<?php echo $this->params['tarefa']->getId() ?>">

        <div class="row">
          <div class="col-md-12">
            <button type="submit" class="col-md-2 btn btn-lg btn-success btn-block mx-1 float-right">Salvar</button>
            <a href="?controller=Tarefa&action=listar"><button class="col-md-2 btn btn-lg btn-default btn-block mx-1 float-right">Cancelar</button></a>
          </div>
        </div>
      </form>
    </div>
  </div>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

</body>
</html>