<?php

class TarefaModel extends Database {

  private $id;
  private $prioridade;
  private $prazo;
  private $titulo;
  private $nomeResponsavel;
  private $status;

  private $table = 'tarefas';

  public function setId($id){
    $this->id = $id;
    return $this;
  }

  public function getId(){
    return $this->id;
  }

  public function setPrioridade($prioridade){
    $this->prioridade = $prioridade;
    return $this;
  }

  public function getPrioridade(){
    return $this->prioridade;
  }

  public function setPrazo($prazo){
    $this->prazo = $prazo;
    return $this;
  }

  public function getPrazo(){
    return $this->prazo;
  }

  public function setTitulo($titulo){
    $this->titulo = $titulo;
    return $this;
  }

  public function getTitulo(){
    return $this->titulo;
  }

  public function setNomeResponsavel($nomeResponsavel){
    $this->nomeResponsavel = $nomeResponsavel;
    return $this;
  }

  public function getNomeResponsavel(){
    return $this->nomeResponsavel;
  }

  public function setStatus($status){
    $this->status = $status;
    return $this;
  }

  public function getStatus(){
    return $this->status;
  }

  public function getAll(){

    $query = "SELECT * FROM `$this->table`";

    $tarefas = array();

    $result = $this->mysqli->query($query);

    while($row = $result->fetch_assoc()) {
      $tarefa = new TarefaModel();
      $tarefa->setId($row['id']);
      $tarefa->setTitulo($row['titulo']);
      $tarefa->setPrioridade($row['prioridade']);
      $tarefa->setStatus($row['status']);
      $tarefa->setNomeResponsavel($row['nome_responsavel']);
      $tarefa->setPrazo($row['prazo']);
      array_push($tarefas, $tarefa);
    }

    return $tarefas;
  }

  public function get($id){

    $query = "SELECT * FROM $this->table WHERE id = $id;";

    $result = $this->mysqli->query($query);

    while($row = $result->fetch_assoc()) {
      $this->setId($row['id']);
      $this->setTitulo($row['titulo']);
      $this->setPrioridade($row['prioridade']);
      $this->setStatus($row['status']);
      $this->setNomeResponsavel($row['nome_responsavel']);
      $this->setPrazo($row['prazo']);
    }

    return $this;
  }

  public function save(){

    if(is_null($this->id)){
      $query = "INSERT INTO $this->table (titulo, prioridade, status, nome_responsavel, prazo) VALUES ('$this->titulo', '$this->prioridade', '$this->status', '$this->nomeResponsavel', '$this->prazo');";
    } else {
      $query = "UPDATE $this->table SET titulo = '$this->titulo', prioridade = '$this->prioridade', status = '$this->status', nome_responsavel = '$this->nomeResponsavel', prazo = '$this->prazo' WHERE id = $this->id";
    }

    return $this->mysqli->query($query);
  }

  public function delete(){

    $query = "DELETE FROM $this->table WHERE id = $this->id";

    return $this->mysqli->query($query);
  }

}