<?php

require_once 'models/TarefaModel.php';

class TarefaController {

  public function index() {
    $objTarefaModel = new TarefaModel();

    $tarefas = $objTarefaModel->getAll();

    $objView = new View('tarefa/index', array('tarefas' => $tarefas));
    $objView->showView();
  }

  public function form() {

    $objTarefaModel = new TarefaModel();

    if( isset($_REQUEST['id']) ){
      if( Helper::isInteger($_REQUEST['id']) ){
        $objTarefaModel->get($_REQUEST['id']);
      }
    }

    if(count($_POST) > 0) {
      $objTarefaModel->setTitulo(Helper::cleanString($_POST['titulo']));
      $objTarefaModel->setPrioridade($_POST['prioridade']);
      $objTarefaModel->setStatus($_POST['status']);
      $objTarefaModel->setNomeResponsavel(Helper::cleanString($_POST['nome_responsavel']));
      $objTarefaModel->setPrazo($_POST['prazo']);

      if($objTarefaModel->save()){
        header("Location: ?controller=Tarefa&action=index");
      }
    }

    $objView = new View('tarefa/form', array('tarefa' => $objTarefaModel));
    $objView->showView();
  }

  public function deletar() {
    if( isset($_GET['id']) ){
      if( Helper::isInteger($_GET['id']) ){
        $objTarefaModel = new TarefaModel();
        $objTarefaModel->get($_GET['id']);
        $objTarefaModel->delete();
      }
    }

    header("Location: ?controller=Tarefa&action=index");
  }

}